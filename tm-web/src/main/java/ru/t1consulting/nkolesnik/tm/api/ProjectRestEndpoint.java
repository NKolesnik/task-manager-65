package ru.t1consulting.nkolesnik.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/projects")
public interface ProjectRestEndpoint {

    @PutMapping("/create")
    void create();

    @GetMapping("/existById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findById/{id}")
    Project findById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<Project> findAll();

    @GetMapping("/count")
    long count();

    @PostMapping("/save")
    void save(@RequestBody Project project);

    @PostMapping("/delete")
    void delete(@RequestBody Project project);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<Project> project);

    @DeleteMapping("/clear")
    void clear();

}
