package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ProjectRepository {

    private final Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("PROJ_1"));
        add(new Project("PROJ_2"));
        add(new Project("PROJ_3"));
        add(new Project("PROJ_4"));
    }

    private void add(@NotNull final Project project) {
        projects.put(project.getId(), project);
    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis()));
    }

    public boolean existsById(@NotNull final String id) {
        return projects.containsKey(id);
    }

    @Nullable
    public Project findById(@NotNull final String id) {
        return projects.get(id);
    }

    @Nullable
    public Collection<Project> findAll() {
        return projects.values();
    }

    public long count() {
        return projects.size();
    }

    public void save(@NotNull final Project project) {
        add(project);
    }

    public void deleteById(@NotNull final String id) {
        projects.remove(id);
    }

    public void delete(@NotNull final Project project) {
        projects.remove(project.getId());
    }

    public void deleteAll(@NotNull final List<Project> projectList) {
        projectList.forEach(this::delete);
    }

    public void clear() {
        projects.clear();
    }

}
