package ru.t1consulting.nkolesnik.tm.api;

import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collection;
import java.util.List;

@RequestMapping("/api/tasks")
public interface TaskRestEndpoint {

    @PutMapping("/create")
    void create();

    @GetMapping("/existsById/{id}")
    boolean existsById(@PathVariable("id") String id);

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id);

    @GetMapping("/findAll")
    Collection<Task> findAll();

    @GetMapping("/count")
    long count();

    @PostMapping("/save")
    void save(@RequestBody Task task);

    @PostMapping("/delete")
    void delete(@RequestBody Task task);

    @DeleteMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id);

    @PostMapping("/deleteAll")
    void deleteAll(@RequestBody List<Task> task);

    @DeleteMapping("/clear")
    void clear();

}
