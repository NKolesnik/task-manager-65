package ru.t1consulting.nkolesnik.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1consulting.nkolesnik.tm.api.TaskRestEndpoint;
import ru.t1consulting.nkolesnik.tm.model.Task;
import ru.t1consulting.nkolesnik.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
public class TaskRestEndpointImpl implements TaskRestEndpoint {

    @Autowired
    TaskRepository taskRepository;

    @Override
    @PutMapping("/create")
    public void create() {
        taskRepository.create();
    }

    @Override
    @GetMapping("/existsById/{id}")
    public boolean existsById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.existsById(id);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Task findById(@NotNull @PathVariable("id") final String id) {
        return taskRepository.findById(id);
    }

    @Override
    @GetMapping("/findAll")
    public Collection<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    @GetMapping("/count")
    public long count() {
        return taskRepository.count();
    }

    @Override
    @PostMapping("/save")
    public void save(@NotNull @RequestBody final Task task) {
        taskRepository.save(task);
    }

    @Override
    @PostMapping("/delete")
    public void delete(@NotNull @RequestBody final Task task) {
        taskRepository.delete(task);
    }

    @Override
    @DeleteMapping("/deleteById/{id}")
    public void deleteById(@NotNull final String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void deleteAll(@NotNull @RequestBody final List<Task> tasks) {
        taskRepository.deleteAll(tasks);
    }

    @Override
    @DeleteMapping("/clear")
    public void clear() {
        taskRepository.clear();
    }

}
