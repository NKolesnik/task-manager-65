package ru.t1consulting.nkolesnik.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1consulting.nkolesnik.tm")
public class ApplicationConfiguration {

}
