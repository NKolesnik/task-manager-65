package ru.t1consulting.nkolesnik.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1consulting.nkolesnik.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TaskRepository {

    private final Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("TASK_1"));
        add(new Task("TASK_2"));
        add(new Task("TASK_3"));
        add(new Task("TASK_4"));
    }

    private void add(@NotNull final Task task) {
        tasks.put(task.getId(), task);
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public boolean existsById(@NotNull final String id) {
        return tasks.containsKey(id);
    }

    @Nullable
    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    @Nullable
    public Collection<Task> findAll() {
        return tasks.values();
    }

    public void save(@NotNull final Task task) {
        add(task);
    }

    public void delete(@NotNull final Task task) {
        tasks.remove(task.getId());
    }

    public void deleteById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void deleteAll(@NotNull final List<Task> taskList) {
        taskList.forEach(this::delete);
    }

    public void clear() {
        tasks.clear();
    }

    public long count() {
        return tasks.size();
    }

}
