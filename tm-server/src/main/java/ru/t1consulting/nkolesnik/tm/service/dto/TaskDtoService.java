package ru.t1consulting.nkolesnik.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1consulting.nkolesnik.tm.api.service.dto.ITaskDtoService;
import ru.t1consulting.nkolesnik.tm.dto.model.TaskDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Sort;
import ru.t1consulting.nkolesnik.tm.enumerated.Status;
import ru.t1consulting.nkolesnik.tm.exception.entity.StatusNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.TaskNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.entity.UserNotFoundException;
import ru.t1consulting.nkolesnik.tm.exception.field.*;
import ru.t1consulting.nkolesnik.tm.repository.dto.TaskDtoRepository;

import java.util.*;

@Service
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDto> implements ITaskDtoService {

    @NotNull
    @Autowired
    protected TaskDtoRepository repository;

    @Override
    @Transactional
    public void add(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        repository.saveAndFlush(task);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        task.setUserId(userId);
        repository.saveAndFlush(task);
    }

    @Override
    @Transactional
    public void add(@Nullable final Collection<TaskDto> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Collection<TaskDto> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        for (TaskDto task : tasks) {
            task.setUserId(userId);
        }
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void set(@Nullable final Collection<TaskDto> tasks) {
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        repository.deleteAll();
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    public void set(@Nullable final String userId, @Nullable final Collection<TaskDto> tasks) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (tasks == null || tasks.isEmpty()) throw new TaskNotFoundException();
        repository.deleteAll();
        for (TaskDto task : tasks){
            task.setUserId(userId);
        }
        repository.saveAll(tasks);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name
    ) {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setUserId(userId);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        repository.saveAndFlush(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setName(name);
        task.setUserId(userId);
        task.setDescription(description);
        task.setDateBegin(dateBegin);
        task.setDateEnd(dateEnd);
        repository.saveAndFlush(task);
        return task;
    }

    @Override
    public long getSize() {
        return repository.count();
    }

    @Override
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @NotNull
    @Override
    public List<TaskDto> findAll() {
        @Nullable final List<TaskDto> tasks;
        tasks = repository.findAll();
        if (tasks.isEmpty()) return Collections.emptyList();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final List<TaskDto> tasks;
        tasks = repository.findAll(userId);
        if (tasks.isEmpty()) return Collections.emptyList();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final Comparator<TaskDto> comparator) {
        if (comparator == null) return findAll();
        @Nullable final List<TaskDto> taskList;
        taskList = repository.findAll(org.springframework.data.domain.Sort.by
                (
                    org.springframework.data.domain.Sort.Direction.DESC,
                    getSortColumnName(comparator)
                )
        );
        if (taskList.isEmpty()) return Collections.emptyList();
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final List<TaskDto> taskList;
        taskList = repository.findAll(org.springframework.data.domain.Sort.by
                (
                        org.springframework.data.domain.Sort.Direction.DESC,
                        getSortColumnName(sort.getComparator())
                )
        );
        if (taskList.isEmpty()) return Collections.emptyList();
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId, @Nullable final Comparator<TaskDto> comparator) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll();
        @Nullable final List<TaskDto> taskList;
        taskList = repository.findAll(userId, getSortColumnName(comparator));
        if (taskList.isEmpty()) return Collections.emptyList();
        return taskList;
    }

    @NotNull
    @Override
    public List<TaskDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @Nullable final List<TaskDto> taskList;
        taskList = repository.findAll(userId, getSortColumnName(sort.getComparator()));
        if (taskList.isEmpty()) return Collections.emptyList();
        return taskList;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        if (projectId == null || projectId.isEmpty()) return findAll(userId);
        @Nullable List<TaskDto> result;
        result = repository.findAllByProjectId(userId, projectId);
        if (result.isEmpty()) return Collections.emptyList();
        return result;
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public TaskDto findById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findById(userId, id);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(id);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        if (userId == null || userId.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

    @Override
    @Transactional
    public void update(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        repository.saveAndFlush(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDto task = repository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        update(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (status == null) throw new StatusNotFoundException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();

        @Nullable final TaskDto task = repository.findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        update(task);
    }

    @Override
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    @Transactional
    public void remove(@Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        repository.delete(task);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final TaskDto task) {
        if (task == null) throw new TaskNotFoundException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final String id = task.getId();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        repository.removeByProjectId(userId, projectId);
    }

}
