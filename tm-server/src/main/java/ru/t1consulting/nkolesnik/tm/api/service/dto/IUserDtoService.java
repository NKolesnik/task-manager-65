package ru.t1consulting.nkolesnik.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1consulting.nkolesnik.tm.dto.model.UserDto;
import ru.t1consulting.nkolesnik.tm.enumerated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserDtoService extends IDtoService<UserDto> {

    void add(@Nullable UserDto user);

    void addALl(@Nullable Collection<UserDto> users);

    void set(@Nullable Collection<UserDto> users);

    @NotNull
    UserDto create(@Nullable String userId, @Nullable String name);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @NotNull
    UserDto create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    UserDto create(@Nullable String login, @Nullable String password, @Nullable String email, @Nullable Role role);

    long getSize();

    @Nullable
    List<UserDto> findAll();

    @Nullable
    UserDto findById(@Nullable String id);

    @Nullable
    UserDto findByLogin(@Nullable String login);

    @Nullable
    UserDto findByEmail(@Nullable String email);

    @NotNull
    Boolean isLoginExist(@Nullable String login);

    @NotNull
    Boolean isEmailExist(@Nullable String email);

    void setPassword(@Nullable String id, @Nullable String password);

    @NotNull
    UserDto updateUser(
            @Nullable String id,
            @Nullable String firstName,
            @Nullable String middleName,
            @Nullable String lastName
    );

    void update(@Nullable UserDto user);

    void lockUserByLogin(@Nullable String login);

    void unlockUserByLogin(String login);

    void clear();

    void remove(@Nullable UserDto user);

    void removeByLogin(@Nullable String login);

    void removeById(@Nullable String id);

}
